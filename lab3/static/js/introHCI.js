'use strict';

// Call this function when the page loads (the "ready" event)
$(document).ready(function() {
	initializePage();
})

/*
 * Function that is called when the document is ready.
 */
function initializePage() {
	$('#testjs').click(function(e) {
		$('.jumbotron h1').text("Javascript is connected");
		$('.jumbotron button').text('Please wait');
		$(".jumbotron p").toggleClass("active");
	});

	// Add any additional listeners here
	// example: $("#div-id").click(functionToCall);
	$('a.thumbnail').click(function(e){
		//prevent the page from reloading
		e.preventDefault();
		// In an event handler, $(this) refers to the object 
		// that trigger that triggered the event
		var projectTitle = $(this).find("p").text();
		var jumbotronHeader = $('.jumbotron h1');
		console.log("Number of matching item " + jumbotronHeader.length);
		jumbotronHeader.text(projectTitle);
		var containingProject = $(this).closest('.project');
		var description = $(containingProject).find('.project-description');
		if(description.length == 0){
			containingProject.append("<div class='project-description'><p>Description of the project.</p></div>");	
		}else{
			$(".project-description").toggle();
			$(".project-description").prev().find(".img").toggle();
			//description.html("<p>Stop clicking on me! You just did it at " + (new Date()) + "</p>");
		}
		//$(this).css('background-color','#7fff00');
	});

	$("#submitbn").click(function(e){
		//prevent the page from reloading
		e.preventDefault();
		var projectID = $("#select_project option:selected").val();
		var width_val = $("#width").val();
		var des = $("#description").val();
		if(!width_val){
			$("#Width_label").text("To width (in pixels) Missing: ");
			$('#Width_label').css("color","Red");	
		}
		else{
			$("#Width_label").text("To width (in pixels)");
			$('#Width_label').css("color","");
		}
		if(!des){
			$("#des_label").text("And to description Missing:");
			$('#des_label').css("color","Red");
		}
		else{
			$("#des_label").text("And to description");			
			$('#des_label').css("color","");
		}
		if(des && width_val){
			$(projectID).animate({
				width: width_val
			});
			$(projectID + ' .project-description').text(des);
		}

	});
}

