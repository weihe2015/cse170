'use strict';

// Call this function when the page loads (the "ready" event)
$(document).ready(function() {
	initializePage();
})

/*
 * Function that is called when the document is ready.
 */
function initializePage() {
	console.log("Javascript connected!");
}

function anagrammedName(name) {
	// Thanks, Internet Anagram Server!
	
	if (name == "Doug Engelbart") {
		return "Notable Grudge";
	} 
	else if (name == "Ivan Sutherland") {
		return "Vandal Heist Run";
	}
	else if (name == "JCR Licklider") {
		return "Crick Rid Jell";
	}
	else if (name == "Vannevar Bush") {
		return "Ravens Van Hub";
	}
	else if (name == "Alan C. Kay") {
		return "Canal Yak";
	}
	else if (name == "Allen Newell") {
		return "Ellen All New";
	}
	else if (name == "Lucy Suchman") {
		return "Lunacy Chums";
	}
	else if (name == "Grace Hopper") {
		return "Gear Chopper";
	}
	else {
		console.log(name + " not known for anagramming.");
		return name;
	}
}


$(".person-name").click(function(e){
	e.preventDefault();
	var name = $(this).text();
	var new_name = anagrammedName(name);
	$(this).text(new_name);
});

function checkFriendform(){
	var name = $("#name").val();
	var description = $("#description").val();
	if(name && description){
		$("#lb_name").text("Name: ");
		$('#lb_name').css("color","");
		$("#lb_description").text("Description: ");
		$('#lb_description').css("color","");
		return true;
	}
	if(!name){
		$("#lb_name").text("Name Missing: ");
		$('#lb_name').css("color","Red");
	}
	else{
		$("#lb_name").text("Name: ");
		$('#lb_name').css("color","");
	}
	if(!description){
		$("#lb_description").text("Description Missing");
		$('#lb_description').css("color","Red");
	}
	else{
		$("#lb_description").text("Description: ");
		$('#lb_description').css("color","");
	}
	return false;
}

function checkNewsform(){
	var name = $("#name").val();
	var content = $("#content").val();
	if(name && content){
		$("#lb_name").text("Name: ");
		$('#lb_name').css("color","");
		$("#lb_content").text("Content: ");
		$('#lb_content').css("color","");
		$('#messages').removeClass('hide').addClass('alert alert-success alert-dismissible').slideDown().show();
	    $('#messages_content').html('<h4>Submit successfully</h4>');
	   	$('#modal').modal('show');
		console.log("Return true");
		return true;
	}
	if(!name){
		$("#lb_name").text("Name Missing: ");
		$('#lb_name').css("color","Red");
	}
	else{
		$("#lb_name").text("Name: ");
		$('#lb_name').css("color","");
	}
	if(!content){
		$("#lb_content").text("Content Missing");
		$('#lb_content').css("color","Red");
	}
	else{
		$("#lb_content").text("Content: ");
		$('#lb_content').css("color","");
	}
	return false;	
}
// TODO: bug for this. no way to solve
$('body').on('hidden.bs.modal', '.modal', function () {
	console.log("in hidden");
  $(this).removeData('bs.modal');
});