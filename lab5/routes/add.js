var data = require("../data.json");

exports.addFriend = function(req, res) {  
	// Your code goes here
	var friend_name = req.query.name;
	var friend_description = req.query.description;
	if(friend_name && friend_description){
		var new_friend = {
			'name': friend_name,
			'description': friend_description,
			'imageURL': "http://lorempixel.com/500/500/people/"
		};
		var old_friends = data['friends'];
		data = {
			"friends": old_friends,
			"new_friend": []
		};
		data['new_friend'].push(new_friend);
		data['friends'].push(new_friend);
		res.render('add',data);
		res.redirect('/#'+friend_name);
	}
}