// Get all of our friend data
var data = require('../data.json');

exports.addNews = function(req, res) {  
	// Your code goes here
	var name = req.query.name;
	var news_content = req.query.content;
	var new_News = {
		'name': name,
		'content': news_content,
	};
	data['news'].push(new_News);
	res.render('news',data);
	res.redirect('/news#'+name);
}